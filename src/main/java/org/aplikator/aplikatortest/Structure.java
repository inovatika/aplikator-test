package org.aplikator.aplikatortest;

import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.FunctionResultStatus;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.AnnotationKeys;
import org.aplikator.server.data.ContainerNode;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.PersisterTriggers;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.*;
import org.aplikator.server.query.QueryParameterReference;
import org.aplikator.server.security.Accounts;

import java.math.BigDecimal;
import java.util.Date;

import static org.aplikator.server.descriptor.Panel.column;
import static org.aplikator.server.descriptor.Panel.row;

public class Structure extends Application {


    public static final Publisher publisher = new Publisher();
    public static final Author author = new Author();
    public static final Coauthor coauthor = new Coauthor();
    public static final Book book = new Book();

    static {
        // Reverse collection must be declared only after authors initialization
        author.BOOKS = author.reverseCollectionProperty("AUTHOR_ID", book, book.AUTHOR_ID);

    }

    View viewAuthors;
    View viewAuthorsByName;
    View viewAuthorsMinimal;
    View viewAuthorsWithoutBooks;
    View viewBooks;
    View viewPublishers;
    View viewCoauthors;
    View viewBooksNested;

    @Override
    public void onError(Throwable th, Context ctx) {
        if (th instanceof  IllegalStateException) {
            FunctionResult result = ctx.getRecordContainer().getFunctionResult();
            if (result == null ) {
                result = new FunctionResult("aplikator.table.saveerror", FunctionResultStatus.ERROR);
                result.setDetail("Ilegalni chyba");
            } else if ( !result.getStatus().equals(FunctionResultStatus.ERROR)){
                result.setStatus(FunctionResultStatus.ERROR);
                result.setMessage("aplikator.table.saveerror");
                result.addDetail("Ilegalni chyba");
            }
            ctx.getRecordContainer().setFunctionResult(result);
        }
    }

    @Override
    public void initialize() {
        try {
            System.out.println("ApplicationLoader started");
            Accounts accounts = new Accounts();
            accounts.setAccessControl(AccessControl.Default.authenticated(Access.NONE).role("admin", Access.READ_WRITE_CREATE_DELETE));
            setAccountsEntity(accounts);

            // SERVER SIDE
            System.out.println("ApplicationLoader 1");
            System.out.println("ApplicationLoader 2");
            publisher.setAccessControl(AccessControl.Default.authenticated(Access.NONE).role("admin", Access.READ_WRITE_CREATE_DELETE));
            //book.setAccessControl(AccessControl.Default.authenticatedFullAccess());
            author.setAccessControl(AccessControl.Default.authenticatedFullAccess());
            //book.setAccessControl(AccessControl.Default.allAuthenticated());

            viewPublishers = publisher.view();

            viewAuthors = new View(author, "Default");
            viewAuthors.setPageSize(4);
            viewAuthors.addProperty(author.LASTNAME).addProperty(author.FIRSTNAME).addProperty(author.GENDER, false, false, false).addProperty(author.ACTIVE, true, false, false);
                    //.addProperty(author.BOOKS.relate(book.NAME));
            //.addProperty(author.BOOKS.relate(book.PRICE));

            viewAuthors.addSortDescriptor("Default", author.LASTNAME.getLocalizationKey(), SortItem.ascending(author.LASTNAME));
            viewAuthors.addQueryDescriptor("emptyFirstName", "Empty First Name", author.FIRSTNAME.NULL());

            viewAuthors.addQueryDescriptorToIndex(1,"queryFullName", "Full Name", author.FIRSTNAME.LIKE(QueryParameterReference.param(0))
                            .AND(author.LASTNAME.LIKE(QueryParameterReference.param(1))),
                    new QueryParameter("Jmeno", author.FIRSTNAME),
                    new QueryParameter("Prijmenixxx", author.LASTNAME));
            Function renameFunction = new Function("Rename", "Rename", new NastavitJmeno());
            renameFunction.setAccessControl(AccessControl.Default.authenticated(Access.NONE).role("admin", Access.READ_WRITE_CREATE_DELETE));
            renameFunction.setLocalizationTooltipKey("tooltip.rename");
            viewAuthors.addFunction(renameFunction);
            viewAuthors.addFunction(renameFunction);
            author.setPersistersTriggers(new PersisterTriggers.Default() {
                @Override
                public void onPrepare(ContainerNode node, boolean isCopy, Context ctx) {
                    if (isCopy) {
                        if ("nekopirovat".equalsIgnoreCase(node.getOriginal().getValue(author.LASTNAME))) {
                            FunctionResult fr = new FunctionResult("aplikator.table.saveerror", FunctionResultStatus.ERROR);
                            fr.addDetail("Tohle nejde kopirovat!");
                            ctx.getRecordContainer().setFunctionResult(fr);
                        } else if ("zmenit".equalsIgnoreCase(node.getOriginal().getValue(author.LASTNAME))) {
                            FunctionResult fr = new FunctionResult("aplikator.table.savewarning", FunctionResultStatus.WARNING);
                            fr.addDetail("Tohle by bylo dobre zmenit!");
                            ctx.getRecordContainer().setFunctionResult(fr);
                        } else if ("shodit".equalsIgnoreCase(node.getOriginal().getValue(author.LASTNAME))) {
                            throw new IllegalArgumentException("Shodit sestrelil");
                        }
                    }
                }

                @Override
                public void onLoad(Record record, View view, Context ctx) {
                    Object recVal = record.getValue(author.GENDER);
                    String p = "";
                    if (recVal != null) {
                        if ("M".equalsIgnoreCase(recVal.toString())) {
                            p = "muž";
                        } else {
                            p = "žena";
                        }
                    }
                    Boolean active = record.getValue(author.ACTIVE);
                    if (active != null && active) {
                        record.putPropertyAnnotation(author.LASTNAME, AnnotationKeys.ACCESS_ANNOTATION_KEY, Access.READ.name());
                    }
                    record.setPreview(new SafeHtmlBuilder()
                            /*.appendHtmlConstant("<img src=\"http://krameriusdemo.mzk.cz/search/img/k5.png\">)*/
                            .appendHtmlConstant("<b>").appendEscaped(record.getValue(author.LASTNAME)).appendHtmlConstant("&nbsp;").appendEscaped(record.getValue(author.FIRSTNAME)).appendHtmlConstant("</b><br/>" + p)
                            .toSafeHtml());
                }

                @Override
                public void onUpdate(ContainerNode node, Context ctx) {
                    super.onUpdate(node, ctx);
                    String firstName = node.getEdited().getValue(author.FIRSTNAME);
                    if (firstName != null && firstName.contains("x")) {
                        FunctionResult fr = new FunctionResult("aplikator.table.saveerror", FunctionResultStatus.ERROR);
                        fr.addDetail("To x se ale vůbec nepovedlo!");
                        ctx.getRecordContainer().setFunctionResult(fr);
                    } else if (firstName != null && firstName.contains("w")) {
                        FunctionResult fr = new FunctionResult("aplikator.table.savewarning", FunctionResultStatus.WARNING);
                        fr.addDetail("To w se ale moc nepovedlo!");
                        ctx.getRecordContainer().setFunctionResult(fr);
                    } else if (firstName != null && firstName.contains("z")) {
                        throw new IllegalStateException("To z se ale ani trochu nepovedlo!");
                    } else {
                        ctx.getRecordContainer().setFunctionResult(new FunctionResult("To se ale povedlo!", FunctionResultStatus.SUCCESS));
                    }
                }
            });

            book.setPersistersTriggers(new PersisterTriggers.Default() {
                @Override
                public void onLoad(Record record, View view, Context ctx) {
                    record.setPreview(
                            new SafeHtmlBuilder()
                                    .appendHtmlConstant("<b>").appendEscaped(record.getValue(book.NAME)).appendHtmlConstant("</b><br/>")
                                    .appendHtmlConstant("<b>").appendEscaped(record.getStringValue(book.DATE_OF_PUBL, ctx)).appendHtmlConstant("</b><br/>")
                                    .appendEscaped(record.getValue(book.AUTHOR_ID.relate(author.LASTNAME))).appendHtmlConstant(" ").appendEscaped(record.getValue(book.AUTHOR_ID.relate(author.FIRSTNAME))).appendHtmlConstant("<br/>")
                                    .appendEscaped(record.getStringValue(book.AUTHOR_ID.join(author.PUBLISHER).relate(publisher.COMPANYNAME), ctx))
                                    .toSafeHtml());
                }

                @Override
                public void afterCommit(ContainerNode node, Context ctx) {
                    super.afterCommit(node, ctx);
                    //String bindataID = node.getEdited().getValue(book.PICTURE) != null ? node.getEdited().getValue(book.PICTURE).dataRecordID : "";
                    //Connection conn = ctx.getJDBCConnection();
                    // tady pres SQL najit pripadne existujici zaznamy

                    //ctx.getRecordContainer().setFunctionResult(new FunctionResult("ID:" + bindataID + " Conn:" + conn, FunctionResultStatus.SUCCESS));
                    //ctx.closeTransaction();
                }
            });

            viewAuthorsByName = new View(author, "ByName");
            viewAuthorsByName.setLocalizationKey("AuthorsByNameReadable");
            viewAuthorsByName.addProperty(author.LASTNAME).addProperty(author.FIRSTNAME).addProperty(author.GENDER)
                    .addProperty(author.PUBLISHER.relate(publisher.COMPANYNAME));
            viewAuthorsByName.addQueryDescriptor("Default", "LastnameStartsWith", author.LASTNAME.LIKE(QueryParameterReference.param(0)), new QueryParameter("LastnameStartsWith", author.LASTNAME));
            viewAuthorsByName.addFunction(renameFunction);

            viewAuthorsMinimal = new View(author, "Minimal");
            viewAuthorsMinimal.addProperty(author.LASTNAME).addProperty(author.FIRSTNAME);


            viewAuthorsWithoutBooks = new View(author, "WithoutBooks");
            viewAuthorsWithoutBooks.addProperty(author.LASTNAME).addProperty(author.FIRSTNAME).addProperty(author.GENDER);
            viewAuthorsWithoutBooks.addSortDescriptor("Default", "ByLastname", SortItem.ascending(author.LASTNAME));
            viewAuthorsWithoutBooks.setForm(createAuthorsWithoutBooksForm());

            viewCoauthors = coauthor.view();
            viewCoauthors.setForm(createCoauthorsForm());

            viewBooks = book.view();
            viewBooks.addProperty(book.AUTHOR_ID.relate(author.LASTNAME)).addProperty(book.AUTHOR_ID.relate(author.FIRSTNAME))
                    .addProperty(book.AUTHOR_ID.join(author.PUBLISHER).relate(publisher.COMPANYNAME));
            //.addProperty(book.COAUTHORS.join(coauthor.AUTHOR_ID).relate(author.LASTNAME));
            viewBooks.setForm(createBooksForm());
            viewBooks.setPageSize(4);
            viewBooks.addQueryDescriptor("coauthor.surname", "COAUTHORSURNAME", book.COAUTHORS.join(coauthor.AUTHOR_ID).relate(author.LASTNAME).LIKE(QueryParameterReference.param(0)), new QueryParameter("LastnameStartsWith", author.LASTNAME));

            viewBooksNested = new View(book, "Nested").openAsTable();
            viewBooksNested.addProperty(book.NAME).addProperty(book.AUTHOR_ID.relate(author.LASTNAME)).addProperty(book.AUTHOR_ID.relate(author.FIRSTNAME)).addProperty(book.DATE_OF_PUBL);
            viewBooksNested.setForm(createNestedBooksForm());

            viewAuthors.setForm(createAuthorsForm());

            viewAuthorsByName.setForm(createAuthorsRepeatedForm());

            System.out.println("ApplicationLoader 3");
            // CLIENT SIDE MENU
            Menu authors = new Menu(author.getId());
            //authors.addView( viewAuthors);
            authors.addView(viewAuthors);
            authors.addView(viewAuthorsByName);

            // authors.addAction(new CDAction("Create", new
            // CreateEntity(authors.getName() /*+ " - Create"*/, authors)));
            // authors.addAction(new ActionDTO("Some Function", new
            // SomeFunction("SomeFunction", authors)));
            Menu books = new Menu(book.getId());
            books.addView(viewBooks);
            addMenu(authors);
            addMenu(books);

            Menu publ = new Menu(publisher.getId());
            publ.addView(viewPublishers);
            publ.addView(accounts.view());
            addMenu(publ);

            System.out.println("ApplicationLoader finished");
            //Search s = SearchFactory.get();
            //System.out.println("Search started!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+s);
        } catch (Exception ex) {
            System.out.println("ApplicationLoader error:" + ex);
            throw new RuntimeException("ApplicationLoader error: ", ex);
        }
    }

    private Form createAuthorsForm() {
        Form form = new Form(true);
        Panel nameRow = row(new TextField<String>(author.LASTNAME).setSize(3), new TextField<String>(author.FIRSTNAME).setSize(5)).setFrame(true);
        nameRow.setLocalizationKey("bububu");
        form.setLayout(column(
                row(
                        column(nameRow).setSize(6),
                        column(row(new ComboBox<String>(author.GENDER))).setSize(6)
                ),
                row(new NestedTable(author.BOOKS, viewBooksNested).setEnabled(true)),
                row(new CheckBox(author.ACTIVE))
        ));
        return form;
    }

    private Form createAuthorsRepeatedForm() {
        Form form = new Form(false);
        form.setLayout(column(
                row(new TextField<String>(author.LASTNAME).setSize(8), new TextField<String>(author.FIRSTNAME).setSize(4)).setFrame(true).setLocalizationKey("author.name").setLocalizationTooltipKey("tooltip.author.name"),
                row(new RadioSwitch<String>(author.GENDER).setElementId("prvniswitch").setHorizontal(false).setSize(6), new CheckBox(author.ACTIVE).setHorizontal(true).setSize(6)),
                row(new RadioSwitch<String>(author.GENDER).setElementId("druhejswitch").setHorizontal(true).setSize(6), new CheckBox(author.ACTIVE).setHorizontal(false).setSize(6)),
                row(new RadioButton<String>(author.GENDER).setHorizontal(true).setSize(6), new RadioButton<String>(author.GENDER).setHorizontal(false).setSize(6)),
                row(new ComboBox<String>(author.GENDER).setSwitcher("M|PANSKY1,PANSKY2|DAMSKY1,DAMSKY2$F|DAMSKY1,DAMSKY2|PANSKY1,PANSKY2").setSize(2)),
                row(ReferenceField.reference(author.PUBLISHER).setLocalizationTooltipKey("tooltip.author.publisher")),
                row(new RepeatedForm(author.BOOKS, viewBooksNested).setSize(9)).setElementId("SKRYVACIPANEL"),
                row(new TextField<String>(author.LASTNAME).setLocalizationTooltipKey("tooltip.pansky1")).setLocalizationKey("PANSKY1").setElementId("PANSKY1").setCustomLabelStyle("pansky"),
                row(new TextField<String>(author.LASTNAME)).setLocalizationKey("DAMSKY1").setElementId("DAMSKY1"),
                row(new TextField<String>(author.LASTNAME)).setLocalizationKey("PANSKY2").setElementId("PANSKY2").setCustomLabelStyle("pansky"),
                row(new TextField<String>(author.LASTNAME)).setLocalizationKey("DAMSKY2").setElementId("DAMSKY2")
        ));
        return form;
    }

    private Form createAuthorsWithoutBooksForm() {
        Form form = new Form(true);
        //form.setLayout(column(author.LASTNAME, new TextFieldCombo<>(author.FIRSTNAME), author.GENDER, author.PUBLISHER.relate(publisher.COMPANYNAME)));
        form.setLayout(column(author.LASTNAME, author.FIRSTNAME, author.GENDER, author.PUBLISHER.relate(publisher.COMPANYNAME)));
        return form;
    }

    private Form createBooksForm() {
        DateField df = new DateField(book.DATE_OF_PUBL);
        //df.setFormatPattern("dd.mm.yyyy");
        Function increPrice = new IncreaseBookPrice();
        increPrice.setLocalizationTooltipKey("tooltip.inceasePrice");
        increPrice.setAccessControl(AccessControl.Default.authenticated(Access.NONE).role("admin", Access.READ_WRITE_CREATE_DELETE));
        Form form = new Form(false);
        form.setLayout(column().add(new TextField<String>(book.NAME))
                .add(book.AUTHOR_ID.join(author.PUBLISHER).relate(publisher.COMPANYNAME))
                .add(ReferenceField.reference(book.AUTHOR_ID, viewAuthorsWithoutBooks/* author.FIRSTNAME, author.LASTNAME*/))
                .add(df).add(new BinaryField(book.PICTURE).setShowTempFile(true)).add(new TextArea(book.ABSTRACT).setHeight(500))
                .add(row(new TextField<BigDecimal>(book.PRICE).setFormatPattern("###,###,000.00"), increPrice))
                .add(new RepeatedForm(book.COAUTHORS, viewCoauthors).setSize(9)));
        return form;
    }

    private Form createCoauthorsForm() {
        Form form = new Form(false);
        form.setLayout(column().add(ReferenceField.reference(coauthor.AUTHOR_ID, author.LASTNAME, author.FIRSTNAME))
                .add(new TextField<String>(coauthor.ROLE))
                .add(new BinaryField(coauthor.ILLUSTRATION)));
        return form;
    }

    private Form createNestedBooksForm() {
        Form form = new Form(false);
        form.setLayout(row(book.NAME, book.PRICE, new DateField(book.DATE_OF_PUBL).setFormatPattern("yyyy-MM-dd HH:mm").setEnabled(true)));
        return form;
    }

    public static class Publisher extends Entity {
        public final Property<String> COMPANYNAME = stringProperty("COMPANYNAME");

        public Publisher() {
            super("Publisher", "PUBLISHER", "PUBLISHER_ID");
            addIndex("PUB_NAME_IDX", false, COMPANYNAME);
        }
    }

    public static class Author extends Entity {
        public static final ListProvider LIST_ANO_NE = new ListProvider.Default("active", new ListItem.Default(false, "active.false"), new ListItem.Default(true, "active.true")).sortByName();
        public final Property<String> FIRSTNAME = stringProperty("FIRSTNAME", 40);//.setListProvider(new ListProvider.Default("firstnames", new ListItem.Default("P", "Pepa"), new ListItem.Default("F", "Franta")).sortByName());
        ;
        public final Property<String> LASTNAME = stringProperty("LASTNAME", 40,true);
        public final Property<Boolean> ACTIVE = booleanProperty("ACTIVE").setListProvider(LIST_ANO_NE);
        @SuppressWarnings("unchecked")
        public final Property<String> GENDER = stringProperty("GENDER", 1).setListProvider(new ListProvider.Default("genders", new ListItem.Default("M", "male"), new ListItem.Default("F", "female")).sortByName());
        public Collection<Book> BOOKS;
        public Reference<Publisher> PUBLISHER = referenceProperty(publisher, "PUBLISHER");

        public Author() {
            super("Author", "AUTHORS", "AUTHOR_ID");
            //FIRSTNAME = stringProperty("FIRSTNAME", 40);
            //LASTNAME = stringProperty("LASTNAME", 40);
            //ACTIVE = booleanProperty("ACTIVE");
            //GENDER = stringProperty("GENDER", 1).setListValues(new ListItem<String>("M", "male"), new ListItem<String>("F", "female"));
            addIndex("AUTHOR_NAME_IDX", true, FIRSTNAME, LASTNAME);
            setIndexed(true);

        }

    }

    public static class Coauthor extends Entity {
        public final Reference<Author> AUTHOR_ID = referenceProperty(author, "AUTHOR_ID");
        public final Property<String> ROLE = stringProperty("ROLE");
        public final BinaryProperty ILLUSTRATION = binaryProperty("ILLUSTRATION");

        public Coauthor() {
            super("Coauthor", "COAUTHORS", "COAUTHOR_ID");
        }
    }

    public static class Book extends Entity {
        public final Property<String> NAME;
        public final Property<String> ABSTRACT;
        public final Property<Date> DATE_OF_PUBL;
        public final Reference<Author> AUTHOR_ID;
        public final Property<BigDecimal> PRICE;
        public final BinaryProperty PICTURE;
        public final Collection<Coauthor> COAUTHORS;

        public Book() {
            super("Book", "BOOKS", "BOOK_ID");
            NAME = stringProperty("NAME", 80);
            ABSTRACT = textProperty("ABSTRACT");//.setEditable(false);
            DATE_OF_PUBL = dateProperty("DATE_OF_PUBL");
            AUTHOR_ID = referenceProperty(author, "AUTHOR_ID");
            PRICE = numericProperty("PRICE").setEncoder((strParam)->"drahy".equals(strParam)?new BigDecimal(100):new BigDecimal(10));
            PICTURE = binaryProperty("PICTURE");
            COAUTHORS = collectionProperty(coauthor, "COAUTHOR", "BOOK_ID");
            addIndex("BOOK_NAME_IDX", true, NAME);

            //DATE_OF_PUBL.setFormatPattern("dd.MM.yyyy  HH:mm");

            PICTURE.setAccessControl(AccessControl.Default.authenticated(Access.READ_PREVIEW).role("admin",Access.READ_WRITE_CREATE_DELETE));
        }

    }

}
