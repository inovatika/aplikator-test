package org.aplikator.aplikatortest;

import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.FunctionResultStatus;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Executable;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.aplikator.server.descriptor.Panel.column;
import static org.aplikator.server.descriptor.Panel.row;

/**
 *
 */
public class IncreaseBookPrice extends Function {


    Property<String> status1;
    Property<String> status2;
    Property<BigDecimal> coefficient;
    Property<Date> datumZdrazeni;

    public IncreaseBookPrice() {
        super("increaseBookPrice", "increaseBookPrice", new Executor());
        WizardPage p1 = new WizardPage(this, "firstPage");
        Property<String> p1input = p1.stringProperty("firstField");
        status1 = p1.stringProperty("status1");
        datumZdrazeni = p1.dateProperty("datumZDrazeni");
        p1.form(row(
                column(new SimpleLabel<String>(status1).setSize(12), p1input, datumZdrazeni)
        ), false);

        WizardPage p2 = new WizardPage(this, "secondPage");
        status2 = p1.stringProperty("status2");
        coefficient = p2.numericProperty("coefficient");

        p2.form(row(
                column(new SimpleLabel<String>(status2).setSize(12), coefficient)
        ), false);
    }


    private static class Executor extends Executable {

        @Override
        public FunctionResult execute(Record currentRecord, Record wizardParameters, ClientContext clientContext, Context ctx) {

            IncreaseBookPrice func = (IncreaseBookPrice) function;

            Structure struct = (Structure) Application.get();
            try {

                List<Record> coauthors = currentRecord.getCollectionRecords(struct.book.COAUTHORS, ctx);
                Record newCoauthor = struct.book.COAUTHORS.createCollectionRecord(currentRecord);
                Record author = currentRecord.getReferredRecord(struct.book.AUTHOR_ID, struct.viewAuthorsMinimal, ctx);
                BigDecimal coef = wizardParameters.getValue(func.coefficient);
                if (coef.doubleValue()>=100){
                    throw new IllegalArgumentException("Příliš velké zdražení");
                }
                BigDecimal currPrice = currentRecord.getValue(struct.book.PRICE);
                currentRecord.setValue(struct.book.PRICE, currPrice.multiply(coef));
                ctx.addUpdatedRecordToContainer(currentRecord, currentRecord);
                ctx.processRecordContainer();

                FunctionResult fr = new FunctionResult("Kniha " + currentRecord.getValue(struct.book.NAME) + " byla zdražena.", FunctionResultStatus.SUCCESS);
                return fr;
            } catch (Throwable t) {

                FunctionResult fr = new FunctionResult("Kniha " + currentRecord.getValue(struct.book.NAME) + " nebyla zdražena." , FunctionResultStatus.ERROR);
                fr.addDetail(t.getMessage());
                return fr;
            }
        }

        @Override
        public WizardPage getWizardPage(String currentPageId, boolean forwardDirection, Record currentRecord, Record wizardParameters, ClientContext clientContext, Context context) {
            WizardPage page = super.getWizardPage(currentPageId, forwardDirection, currentRecord, wizardParameters, clientContext, context);
            IncreaseBookPrice func = (IncreaseBookPrice) function;
            if ("firstPage".equals(page.getPageId())) {
                String currentStatus = wizardParameters.getValue(func.status1);
                if (currentStatus == null) {
                    wizardParameters.setValue(func.status1, "Wizard spusten - prvnistrana");
                } else {
                    wizardParameters.setValue(func.status1, "Navrat na prvni stranu");
                }
            } else if ("secondPage".equals(page.getPageId())) {
                BigDecimal coef = wizardParameters.getValue(func.coefficient);
                if (coef == null) {
                    wizardParameters.setValue(func.status2, "Poprve na druhe strane");
                    wizardParameters.setValue(func.coefficient, new BigDecimal("10"));
                } else {
                    wizardParameters.setValue(func.status2, "Znovu na druhe strane");
                }
            }
            page.setClientRecord(wizardParameters);
            return page;
        }
    }
}