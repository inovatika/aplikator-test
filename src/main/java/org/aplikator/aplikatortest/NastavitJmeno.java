package org.aplikator.aplikatortest;

import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.FunctionResultStatus;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.QueryDescriptorDTO;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Executable;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.Property;
import org.aplikator.server.descriptor.SortItem;
import org.aplikator.server.descriptor.WizardPage;

import java.util.List;
import java.util.logging.Logger;


public class NastavitJmeno extends Executable {
    private static final String NAME = "NastavitJmeno";

    Logger logger = Logger.getLogger(NastavitJmeno.class.getName());


    private QueryDescriptorDTO queryDescriptor;

    @Override
    public FunctionResult execute(Record currentRecord, Record wizardParameters, ClientContext clientContext, Context context) {
        try {

            logger.info("STARTED READING AUTHORS ");
            int counter = 0;
            for (RecordDTO autorDTO : clientContext.getSelectedRecords()) {
                Record autor = new Record(autorDTO);
                logger.info("RECORD:" + autor.getPrimaryKey().getId() + " - " + autor.getStringValue(Structure.author.LASTNAME, context));
                Record autorCLone = autor.clone();
                autorCLone.setValue(Structure.author.FIRSTNAME,"KRESTNI"+counter++);
                context.addUpdatedRecordToContainer(autor, autorCLone);
            }
            context.processRecordContainer();
            context.clearRecordContainer();
            logger.info("FINISHED READING AUTHORS");

            logger.info("GROUPED AUTHORS");
            List<Record> groupedAuthors = context.getRecords(Structure.author).withQuery(Structure.author.LASTNAME.EQUAL(Structure.author.FIRSTNAME)).withSort(SortItem.ascending(Structure.author.FIRSTNAME)).withGroupBy(Structure.author.LASTNAME, Structure.author.FIRSTNAME).list();
            for (Record group : groupedAuthors) {
                logger.info("FN - " + group.getStringValue(Structure.author.FIRSTNAME,context)+":"+group.getValue(Property.COUNT));
            }
            logger.info("GROUPED AUTHORS END");

            return new FunctionResult("Přejmenováno", FunctionResultStatus.SUCCESS);
        } catch (Throwable t) {

            return new FunctionResult("Přejmenování nebylo spuštěno: " + t, FunctionResultStatus.ERROR);
        }

    }

    @Override
    public WizardPage getWizardPage(String currentPage, boolean forwardFlag, Record currentRecord, Record wizardParameters, ClientContext clientContext, Context context) {
        return null;
    }


}

