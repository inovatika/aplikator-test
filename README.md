Aplikator test project 

Build & run instructions:
- checkout the aplikator framework dependency from https://github.com/incad/aplikator
- install aplikator.jar to local gradle repository running ./gradlew install in aplikator root folder
- build relief4 test with ./gradlew build
- create local postgresql database relief4 (user relief4, password relief4)
- copy relief4.war to tomcat/webapps, start tomcat. If postgresql and tomcat run both at localhost, no configuration is necessary
- open http://localhost:8080/relief4/ddl?update=true - it will generate and execute database ddl script
- test application is ready at http://localhost:8080/relief4
